cmake_minimum_required(VERSION 3.19.0)

project(SimpleVulkanRenderer)

set(CMAKE_CXX_STANDARD 20)

find_package(Vulkan REQUIRED)

find_package(SDL2 REQUIRED)
if(NOT TARGET SDL2::SDL2)
    # using old SDL2 cmake, lets create a SDL2 target ourselves
    find_library(SDL2_LIBRARY_FILE_LOCATION SDL2 REQUIRED)

    add_library(SDL2::SDL2 SHARED IMPORTED)
    set_target_properties(SDL2::SDL2 PROPERTIES
        INTERFACE_INCLUDE_DIRECTORIES ${SDL2_INCLUDE_DIRS}
        INTERFACE_LINK_LIBRARIES ${SDL2_LIBRARIES}
        IMPORTED_LOCATION ${SDL2_LIBRARY_FILE_LOCATION}
    )

    unset(SDL2_LIBRARY_FILE_LOCATION)
endif()

find_package(Threads REQUIRED)
find_package(PkgConfig REQUIRED)
pkg_check_modules(JSONCPP jsoncpp)

add_subdirectory(src/shaders)
add_subdirectory(src/tutorial)

add_executable(${PROJECT_NAME}
    src/main.cpp
    src/utils.hpp
    src/utils.cpp
    src/renderer.hpp
    src/renderer.cpp
    src/vulkan_loader_storage.cpp
    src/Vertex.hpp
    src/Buffer.hpp
    src/Buffer.cpp
    src/shaders/shader.hpp
    src/shaders/shader.cpp
)

add_dependencies(${PROJECT_NAME} shaders)

target_link_libraries(${PROJECT_NAME} PUBLIC
    Vulkan::Vulkan
    SDL2::SDL2
    Threads::Threads
    ${JSONCPP_LIBRARIES}
)

target_compile_definitions(${PROJECT_NAME} PUBLIC
    VULKAN_HPP_DISPATCH_LOADER_DYNAMIC=1
)

