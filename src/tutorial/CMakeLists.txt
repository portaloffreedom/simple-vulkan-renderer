#find_package(glfw3 3.3 REQUIRED)

add_executable(15_hello_triangle
    15_hello_triangle.cpp)
add_dependencies(15_hello_triangle shaders)
target_link_libraries(15_hello_triangle PUBLIC
    Vulkan::Vulkan
    # glfw
    SDL2::SDL2
    Threads::Threads
    ${JSONCPP_LIBRARIES}
)
