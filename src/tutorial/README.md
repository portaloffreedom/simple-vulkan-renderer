# Tutorial files

Tutorial files are from [the vulkan tutorial](https://vulkan-tutorial.com/)
and therefore follow their own license:

> The contents of this repository are licensed as CC BY-SA 4.0, unless stated otherwise. By contributing to this repository, you agree to license your contributions to the public under that same license.

> The code listings in the code directory are licensed as CC0 1.0 Universal. By contributing to that directory, you agree to license your contributions to the public under that same public domain-like license.
