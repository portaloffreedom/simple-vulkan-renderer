#include "renderer.hpp"
#include <SDL.h>
#include <iostream>

int main(int argc, char* argv[]) {
    std::cout << "Hello Vulkan" << std::endl;

    Renderer rend;

    int ret = rend.loop();

    return ret;
}
