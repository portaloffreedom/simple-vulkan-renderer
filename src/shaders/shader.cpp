#include "shader.hpp"
#include "../utils.hpp"
#include <cstdint>
#include <vector>
#include <fstream>

Shader::Shader(vk::Device device, const std::string& vert_filename, const std::string& frag_filename)
    : device(device)
{
    std::vector<char> vert_spirv = Utils::read_file(vert_filename);
    std::vector<char> frag_spirv = Utils::read_file(frag_filename);

    vert_module = _create_shader_module(device, std::move(vert_spirv));
    frag_module = _create_shader_module(device, std::move(frag_spirv));
}

Shader::~Shader()
{
    device.destroyShaderModule(vert_module);
    device.destroyShaderModule(frag_module);
}


vk::ShaderModule Shader::_create_shader_module(vk::Device device, const std::vector<char> code)
{
    vk::ShaderModuleCreateInfo createInfo({}, code.size(), reinterpret_cast<const uint32_t*>(code.data()));
    return device.createShaderModule(vk::ShaderModuleCreateInfo(
        {}, code.size(), reinterpret_cast<const uint32_t*>(code.data())
    ));
}

