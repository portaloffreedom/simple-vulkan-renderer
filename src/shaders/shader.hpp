#pragma once
#include <vulkan/vulkan.hpp>
#include <string>

class Shader {
private:
    vk::Device device;
public:
    vk::ShaderModule vert_module, frag_module;
public:
    Shader(vk::Device device, const std::string& vert_filename, const std::string& frag_filename);
    ~Shader();

private:
    vk::ShaderModule _create_shader_module(vk::Device device, const std::vector<char> code);
};
