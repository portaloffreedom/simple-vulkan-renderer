#include "Buffer.hpp"

Buffer::Buffer(vk::Device device,
               vk::PhysicalDevice physical_device,
               vk::DeviceSize size,
               vk::BufferUsageFlags usage,
               vk::MemoryPropertyFlags properties)
    : device(device)
    , bufferSize(size)
{
    buffer = device.createBuffer(vk::BufferCreateInfo({}, size, usage, vk::SharingMode::eExclusive, nullptr, nullptr));

    vk::MemoryRequirements memRequirements = device.getBufferMemoryRequirements(buffer);

    //TODO memory allocations are very limited, this call should be replaced with a library like VulkanMemoryAllocator (https://github.com/GPUOpen-LibrariesAndSDKs/VulkanMemoryAllocator)
    bufferMemory = device.allocateMemory(vk::MemoryAllocateInfo(
        memRequirements.size, find_memory_type(physical_device, memRequirements.memoryTypeBits, properties)
    ));

    device.bindBufferMemory(buffer, bufferMemory, 0);
}

//copy is not allowed, but move is.
Buffer::Buffer(Buffer&& other)
: device(other.device)
, buffer(other.buffer)
, bufferMemory(other.bufferMemory)
, bufferSize(other.bufferSize)
{
    other.buffer = nullptr;
    other.bufferMemory = nullptr;
}

Buffer& Buffer::operator=(Buffer&& other)
{
    destroy();

    device  = other.device;
    buffer  = other.buffer;
    bufferMemory = other.bufferMemory;
    bufferSize = other.bufferSize;

    other.buffer = nullptr;
    other.bufferMemory = nullptr;
    return *this;
}

Buffer::~Buffer()
{
    if (buffer)
        device.destroyBuffer(buffer);
    if (bufferMemory)
        device.freeMemory(bufferMemory);
}

void Buffer::destroy()
{
    if (buffer) {
        device.destroyBuffer(buffer);
        buffer = nullptr;
    }
    if (bufferMemory) {
        device.freeMemory(bufferMemory);
        bufferMemory = nullptr;
    }
}

uint32_t Buffer::find_memory_type(vk::PhysicalDevice physical_device, uint32_t typeFilter, vk::MemoryPropertyFlags properties)
{
    vk::PhysicalDeviceMemoryProperties mem_properties = physical_device.getMemoryProperties();
    for (uint32_t i=0; i < mem_properties.memoryTypeCount; i++) {
        if ((typeFilter & (1 << i)) && (mem_properties.memoryTypes[i].propertyFlags & properties) == properties) {
            return i;
        }
    }

    throw std::runtime_error("failed to find suitable memory type");
}

void Buffer::copyFrom(const Buffer& from, vk::CommandPool &pool, vk::Queue &queue)
{
    assert(bufferSize >= from.bufferSize);

    std::vector<vk::CommandBuffer> command = device.allocateCommandBuffers(vk::CommandBufferAllocateInfo(
        pool, vk::CommandBufferLevel::ePrimary, 1
    ));
    command[0].begin(vk::CommandBufferBeginInfo(vk::CommandBufferUsageFlagBits::eOneTimeSubmit, nullptr));
    command[0].copyBuffer(
        from.buffer,
        this->buffer,
        vk::BufferCopy(0, 0, from.bufferSize));
    command[0].end();

    queue.submit(vk::SubmitInfo(nullptr, nullptr, command, nullptr));
    queue.waitIdle();

    device.freeCommandBuffers(pool, command);
}

Buffer::MappedBuffer::MappedBuffer(Buffer& buffer)
    :buffer(&buffer)
{
    map();
}

Buffer::MappedBuffer::~MappedBuffer()
{
    unmap();
}

Buffer::MappedBuffer::MappedBuffer(MappedBuffer&& other)
    :buffer(other.buffer)
    ,data_ptr(other.data_ptr)
{
    other.data_ptr = nullptr;
}

Buffer::MappedBuffer& Buffer::MappedBuffer::operator=(MappedBuffer&& other)
{
    unmap();

    buffer = other.buffer;
    data_ptr = other.data_ptr;

    other.data_ptr = nullptr;

    return *this;
}

void Buffer::MappedBuffer::map()
{
    data_ptr = buffer->device.mapMemory(buffer->bufferMemory, 0, buffer->bufferSize, {});
}

void Buffer::MappedBuffer::unmap()
{
    if (data_ptr)
        buffer->device.unmapMemory(buffer->bufferMemory);
}

Buffer::MappedBuffer Buffer::mmap()
{
    return MappedBuffer(*this);
}



