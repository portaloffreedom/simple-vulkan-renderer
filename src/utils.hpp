#pragma once
#include <vector>
#include <string>

namespace Utils {

std::vector<char> read_file(const std::string& filename);

}
