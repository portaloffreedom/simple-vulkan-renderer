#pragma once
#include <vulkan/vulkan.hpp>
#include <vulkan/vulkan_core.h>
#include <vulkan/vulkan_handles.hpp>
#include <optional>
#include <cstdint>
#include "sdl2.hpp"
#include "Vertex.hpp"
#include "Buffer.hpp"

class Renderer {
public:
    struct QueueFamilyIndices {
        std::optional<uint32_t> graphics_family;
        std::optional<uint32_t> compute_family;
        std::optional<uint32_t> transfer_family;
        std::optional<uint32_t> present_family;

        bool is_complete() const {
            return graphics_family.has_value()
                && compute_family.has_value()
                && transfer_family.has_value()
                && present_family.has_value();
        }
    };

    struct SwapChainSupportDetails {
        vk::SurfaceCapabilitiesKHR capabilities;
        std::vector<vk::SurfaceFormatKHR> formats;
        std::vector<vk::PresentModeKHR> presentModes;
    };

    constexpr static const uint32_t MAX_FRAMES_IN_FLIGHT = 2;

    const std::vector<Vertex> vertices = {
        {{-0.5f, -0.5f}, {1.0f, 0.0f, 0.0f}},
        {{0.5f, -0.5f},  {0.0f, 1.0f, 0.0f}},
        {{0.5f, 0.5f},   {0.0f, 0.0f, 1.0f}},
        {{-0.5f, 0.5f},  {1.0f, 1.0f, 1.0f}}
    };
    const std::vector<uint16_t> indices = {
        0, 1, 2, 2, 3, 0
    };

private:
    sdl2::sdlsystem_ptr_t system;
    sdl2::window_ptr_t window;
    vk::DynamicLoader dl;
    vk::Instance instance = nullptr;
    vk::SurfaceKHR surface = nullptr;
    vk::PhysicalDevice physical_device = nullptr;
    vk::Device device = nullptr;
    vk::Queue graphics_queue = nullptr;
    vk::Queue present_queue = nullptr;
    vk::Queue compute_queue = nullptr;
    vk::Queue transfer_queue = nullptr;

    vk::SwapchainKHR swapchain = nullptr;
    vk::Format swapchain_image_format;
    vk::Extent2D swapchain_extent;
    std::vector<vk::Image> swapchain_images;
    std::vector<vk::ImageView> swapchain_imageviews;
    bool will_recreate_swap_chain = false;

    vk::PipelineLayout pipelineLayout = nullptr;
    vk::RenderPass renderPass = nullptr;
    vk::Pipeline graphicsPipeline = nullptr;
    std::vector<vk::Framebuffer> swapChainFramebuffers;
    /// memory for commands
    vk::CommandPool commandPool = nullptr;
    std::vector<vk::CommandBuffer> commandBuffers;
    std::array<vk::Semaphore, MAX_FRAMES_IN_FLIGHT> imageAvailableSemaphores;
    std::array<vk::Semaphore, MAX_FRAMES_IN_FLIGHT> renderFinishedSemaphores;
    std::array<vk::Fence, MAX_FRAMES_IN_FLIGHT> inFlightFences;
    uint32_t currentFrame = 0;

    Buffer vertexBuffer;
    Buffer indexBuffer;

    bool enableValidationLayers = true;

    vk::DebugUtilsMessengerEXT debugMessenger;
    const uint32_t verbositiy_level = VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT;

    constexpr static const std::array VALIDATION_LAYERS {
        "VK_LAYER_KHRONOS_validation"
    };
    constexpr static const std::array DEVICE_EXTENSIONS {
        VK_KHR_SWAPCHAIN_EXTENSION_NAME
    };


public:
    Renderer();
    ~Renderer();

    int loop();

private:
    /// Initializes all Vulkan bits and pieces
    void init_renderer();

    /// Creates Vulkan instance
    void _create_instance();
    /// Sets up the debugging messages if validation layers are enabled
    void _setup_debug_messenger();
    /// Creates the debugging messages configuration
    vk::DebugUtilsMessengerCreateInfoEXT _configure_debug_messenger();
    /// Defines extensions required by us
    std::vector<const char*> get_required_extensions() const;

    /// Creates the surface for the swapchain
    void _create_surface();

    /// Picks suitable GPU
    void _pick_physical_device();
    /// Gives an integer value to the device. We are going to pick the device with the higher value.
    int _rate_device_suitability(const vk::PhysicalDevice device) const;
    /// Returns the queue families indexes supported by the GPU
    QueueFamilyIndices _find_queue_families(vk::PhysicalDevice device) const;
    /// Checks if physical device supports
    bool _check_device_extensions_support(vk::PhysicalDevice device) const;

    /// Creates abstract Vulkan device
    void _create_logical_device();

    /// (Re)Creates the swap chain
    void _recreate_swap_chain();
    /// Cleanups old swapchain data
    void _cleanup_swap_chain();
    /// Creates the swap chain
    void _create_swap_chain();
    /// Returns swap chain capabilities
    SwapChainSupportDetails _query_swapchain_support(vk::PhysicalDevice device) const;
    /// choose best surface format
    vk::SurfaceFormatKHR _choose_swapsurface_format(std::vector<vk::SurfaceFormatKHR> &availableFormats) const;
    /// choose best present mode
    vk::PresentModeKHR _choose_swapsurface_present_mode(std::vector<vk::PresentModeKHR> &availablePresentModes) const;
    ///TODO documentation
    vk::Extent2D _choose_swap_extent(const vk::SurfaceCapabilitiesKHR &capabilities) const;

    /// create imageviews for swapchain images
    void _create_swapchain_imageviews();

    /// creates the graphics pipeline
    void _create_graphics_pipeline();
    /// create render pass
    void _create_render_pass();

    /// create framebuffers
    void _create_framebuffers();

    /// Create command buffer memory pool
    void _create_command_pool();
    /// Create command buffer
    void _create_command_buffers();
    ///
    void _create_vertex_buffer();
    ///
    void _create_index_buffer();
    ///
    void _create_sync_objects();

    /// Requests the render of one frame
    void render();

    void _record_command_buffer(vk::CommandBuffer& commandBuffer, uint32_t imageIndex);
    //void _record_command_buffer2(VkCommandBuffer commandBuffer, uint32_t imageIndex);

    /// Requests all inputs since last frame
    bool poll_events();

    /// Vulkan callback to display debug messages
    static VKAPI_ATTR vk::Bool32 VKAPI_CALL debugCallback(
        VkDebugUtilsMessageSeverityFlagBitsEXT messageSeverity,
        VkDebugUtilsMessageTypeFlagsEXT messageType,
        const VkDebugUtilsMessengerCallbackDataEXT* pCallbackData,
        void* pUserData);
};
