#pragma once
#include <vulkan/vulkan.hpp>
#include <vulkan/vulkan_enums.hpp>

/**
 * RAII class to manage vulkan buffers
 */
class Buffer {
private:
    vk::Device device = nullptr;
    vk::Buffer buffer = nullptr;
    vk::DeviceMemory bufferMemory = nullptr;
    vk::DeviceSize bufferSize = 0;
public:
    /// Default empty constructor, builds an invalid buffer
    Buffer() = default;
    /// RAII constructor
    Buffer(vk::Device device, vk::PhysicalDevice physical_device, vk::DeviceSize size, vk::BufferUsageFlags usage, vk::MemoryPropertyFlags properties);

    //copy is not allowed, but move is.
    /// Build from temporary value (move constructor)
    Buffer(Buffer&& other);
    /// Copy from temporary value (move assignment)
    Buffer& operator=(Buffer&& other);

    /// Deconstructor
    ~Buffer();
    /// Destroys internal data, useful if destructor would be called too late
    void destroy();

    static uint32_t find_memory_type(vk::PhysicalDevice physical_device,
                                     uint32_t typeFilter,
                                     vk::MemoryPropertyFlags properties);

    vk::Buffer operator*()
    { return buffer; }

    vk::Buffer get_buffer()
    { return buffer; }

    vk::DeviceMemory get_memory()
    { return bufferMemory; }

    void copyFrom(const Buffer& from, vk::CommandPool &pool, vk::Queue &queue);

    /**
     * RAII for mapping the buffer to memory
     */
    class MappedBuffer {
        friend class Buffer;
    private:
        Buffer *buffer;
        void* data_ptr = nullptr;
    protected:
        explicit MappedBuffer(Buffer& buffer);
    public:
        // disable default and copy
        MappedBuffer() = delete;
        MappedBuffer(const MappedBuffer& o) = delete;
        MappedBuffer& operator=(const MappedBuffer& o) = delete;

        ~MappedBuffer();

        MappedBuffer(MappedBuffer&& o);
        MappedBuffer& operator=(MappedBuffer&& o);


        void* data()
        { return data_ptr; }

        void* operator*()
        { return data_ptr; }

        const void* operator*() const
        { return data_ptr; }

    private:
        void map();
        void unmap();
    };

    /**
     * Creates a RAII object which maps vulkan memory to cpu memory.
     * To unmap the memory, simply
     * This does not work with all types of memory, as it uses the
     * `VkMapMemory` vulkan API
     */
    MappedBuffer mmap();
};
