#include "renderer.hpp"
#include <SDL_video.h>
#include <cstdint>
#include <stdexcept>
#include <vulkan/vulkan.hpp>
#include <vulkan/vulkan_core.h>
#include <vulkan/vulkan_enums.hpp>
#include <vulkan/vulkan_handles.hpp>
#include <glm/vec4.hpp>
#include <glm/mat4x4.hpp>
#include <iostream>
#include <vulkan/vulkan_structs.hpp>
#include <vector>
#include <cstring>
#include <map>
#include <set>
#include <optional>
#include "shaders/shader.hpp"
#include "sdl2.hpp"
#include "SDL2/SDL_vulkan.h"
#include "Vertex.hpp"


template<typename T>
bool check_layer_support(const T &layers)
{
    uint32_t layerCount;
    vk::resultCheck(vk::enumerateInstanceLayerProperties(&layerCount, nullptr), "vk::enumerateInstanceLayerProperties count");
    std::vector<vk::LayerProperties> availableLayers(layerCount);
    vk::resultCheck(vk::enumerateInstanceLayerProperties(&layerCount, availableLayers.data()), "vk::enumerateInstanceLayerProperties");

    for (const char* layerName : layers) {
        bool layerFound = false;
        for (const vk::LayerProperties &layer_prop : availableLayers) {
            if (strcmp(layerName, layer_prop.layerName) == 0) {
                layerFound = true;
                break;
            }
        }

        if (!layerFound) {
            std::clog << "failed to find support for layer " << layerName << std::endl;
            return false;
        }
    }

    return true;
}


Renderer::Renderer()
    : system(sdl2::make_sdlsystem(SDL_INIT_VIDEO | SDL_INIT_EVENTS))
    , window(sdl2::make_window(
        "SimpleVulkanRenderer",
        SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
        800, 600,
        SDL_WINDOW_SHOWN | SDL_WINDOW_VULKAN | SDL_WINDOW_RESIZABLE | SDL_WINDOW_ALLOW_HIGHDPI))
{
    if (!system)
        throw sdl2::SDLError("Error creating SDL2 system");
    if (!window)
        throw sdl2::SDLError("Error creating SDL2 window");

    init_renderer();
}


Renderer::~Renderer()
{
    for (vk::Semaphore imageAvailableSemaphore : imageAvailableSemaphores)
        device.destroySemaphore(imageAvailableSemaphore);
    for (vk::Semaphore renderFinishedSemaphore : renderFinishedSemaphores)
        device.destroySemaphore(renderFinishedSemaphore);
    for (vk::Fence inFlightFence : inFlightFences)
        device.destroyFence(inFlightFence);
    if (commandPool)
        device.destroyCommandPool(commandPool);

    _cleanup_swap_chain();

    // destroy before the device
    vertexBuffer.destroy();

    if (graphicsPipeline)
        device.destroyPipeline(graphicsPipeline);
    if (pipelineLayout)
        device.destroyPipelineLayout(pipelineLayout);
    if (renderPass)
        device.destroyRenderPass(renderPass);

    if (device)
        device.destroy();
    if (debugMessenger)
        instance.destroyDebugUtilsMessengerEXT(debugMessenger);
    if (surface)
        instance.destroySurfaceKHR(surface);
    if (instance)
        instance.destroy();
}


void Renderer::init_renderer()
{
    _create_instance();
    _setup_debug_messenger();
    _create_surface();
    _pick_physical_device();
    _create_logical_device();
    _create_swap_chain();
    _create_swapchain_imageviews();
    _create_render_pass();
    _create_graphics_pipeline();
    _create_framebuffers();
    _create_command_pool();
    _create_vertex_buffer();
    _create_index_buffer();
    _create_command_buffers();
    _create_sync_objects();
}


void Renderer::_create_instance()
{
    // Prepare the dynamic dispatcher -----------------------------------------
    PFN_vkGetInstanceProcAddr vkGetInstanceProcAddr = dl.getProcAddress<PFN_vkGetInstanceProcAddr>("vkGetInstanceProcAddr");
    VULKAN_HPP_DEFAULT_DISPATCHER.init(vkGetInstanceProcAddr);

    // Create the vulkan instance ---------------------------------------------
    vk::ApplicationInfo appInfo (
        "Hello Triangle", VK_MAKE_VERSION(1, 0, 0),
        "No Engine", VK_MAKE_VERSION(1, 0, 0),
        VK_API_VERSION_1_0
    );

    std::vector<const char*> instanceLayers;
    std::vector<const char*> instanceExtensions;

    {
        if (enableValidationLayers) {
            if (!check_layer_support(VALIDATION_LAYERS)) {
                throw std::runtime_error("validation layers requested, but not available!");
            }
            instanceLayers.reserve(instanceLayers.size() + VALIDATION_LAYERS.size());
            for (const char* val_layer : VALIDATION_LAYERS) {
                instanceLayers.emplace_back(val_layer);
            }
        }
    }

    {
        std::vector<const char*> required_extensions = get_required_extensions();
        instanceExtensions.reserve(instanceExtensions.size() + required_extensions.size());
        for (auto& x : required_extensions)
        {
            instanceExtensions.push_back(x);
        }
    }

    vk::InstanceCreateInfo createInfo({}, &appInfo, instanceLayers, instanceExtensions);

    vk::DebugUtilsMessengerCreateInfoEXT createDebugMsgInfo = _configure_debug_messenger();
    if (enableValidationLayers) {
        // This adds debug messages for creation and destruction of the vulkan instance
        createInfo.pNext = &createDebugMsgInfo;
    }
    vk::resultCheck(
        vk::createInstance(&createInfo, nullptr, &instance),
        "Failed to create Vulkan instance");

    // Load all vulkan functions ----------------------------------------------
    VULKAN_HPP_DEFAULT_DISPATCHER.init(instance);
}


std::vector<const char*> Renderer::get_required_extensions() const
{
    uint32_t extensionCount;
    SDL_Vulkan_GetInstanceExtensions(window.get(), &extensionCount, nullptr);
    std::vector<const char *> required_extensions(extensionCount);
    SDL_Vulkan_GetInstanceExtensions(window.get(), &extensionCount, required_extensions.data());

    if (enableValidationLayers) {
        required_extensions.reserve(required_extensions.size() + 1);
        required_extensions.emplace_back(VK_EXT_DEBUG_UTILS_EXTENSION_NAME);
    }
    return required_extensions;
}


void Renderer::_create_surface()
{
    VkSurfaceKHR _surface;
    if (!SDL_Vulkan_CreateSurface(window.get(), instance, &_surface)) {
        throw sdl2::SDLError("Error creating a Vulkan surface");
    }
    surface = _surface;
}


void Renderer::_pick_physical_device()
{
    uint32_t device_count = 0;
    vk::resultCheck(instance.enumeratePhysicalDevices(&device_count, nullptr),
                    "Failed to count physical devices");
    if (device_count == 0) {
        throw std::runtime_error("failed to find GPUs with Vulkan support!");
    }
    std::vector<vk::PhysicalDevice> devices(device_count);
    vk::resultCheck(instance.enumeratePhysicalDevices(&device_count, devices.data()),
                    "Failed to enumerate physical devices");


    // Use an ordered map to automatically sort candidates by increasing score
    std::multimap<int, vk::PhysicalDevice> candidates;

    for (const vk::PhysicalDevice& device : devices) {
        int score = _rate_device_suitability(device);
        if (score > 0) {
            candidates.emplace(score, device);
        }
    }
    if (candidates.size() > 0) {
        this->physical_device = candidates.rbegin()->second;
    }

    if (!this->physical_device) {
        throw std::runtime_error("failed to find suitable GPU!");
    }
}


int Renderer::_rate_device_suitability(const vk::PhysicalDevice device) const
{
    // RATES:
    // -1 means not viable
    // >= 0 is the rate, the bigger the better

    vk::PhysicalDeviceProperties props = device.getProperties();
    vk::PhysicalDeviceFeatures features = device.getFeatures();

    // mandatory features (these are examples, we don't need these features really)
    if (!features.geometryShader) return -1;
    if (!features.tessellationShader) return -1;

    // Search for all necessary family queues
    QueueFamilyIndices queue_families = _find_queue_families(device);
    if (!queue_families.is_complete()) return -1;

    if (!_check_device_extensions_support(device/*, DEVICE_EXTENSIONS*/)) return -1;

    SwapChainSupportDetails swapchain_support = _query_swapchain_support(device);
    if (swapchain_support.formats.empty() || swapchain_support.presentModes.empty()) return -1;

    // scores
    int score = 0;
    switch (props.deviceType) {
        // break is missing on purpose
        case vk::PhysicalDeviceType::eDiscreteGpu:
            score+=1000;
        case vk::PhysicalDeviceType::eIntegratedGpu:
            score+=10;
        case vk::PhysicalDeviceType::eVirtualGpu:
            score+=1;
        default:
        case vk::PhysicalDeviceType::eCpu: //if you really want, why should I stop you?
            break;
    }

    // bigger textures is good (self reflection: is it?)
    score += props.limits.maxImageDimension2D;

    return score;
}


Renderer::QueueFamilyIndices Renderer::_find_queue_families(vk::PhysicalDevice device) const
{
    QueueFamilyIndices indices;

    uint32_t queue_family_count = 0;
    device.getQueueFamilyProperties(&queue_family_count, nullptr);
    std::vector<vk::QueueFamilyProperties> queue_families(queue_family_count);
    device.getQueueFamilyProperties(&queue_family_count, queue_families.data());

    for (int i=0; i<queue_family_count; i++) {
        vk::QueueFamilyProperties &props = queue_families[i];
        // Check for queue flags
        if (props.queueFlags & vk::QueueFlagBits::eGraphics) {
            indices.graphics_family = i;
        }
        if (props.queueFlags & vk::QueueFlagBits::eCompute) {
            indices.compute_family = i;
        }
        if (props.queueFlags & vk::QueueFlagBits::eTransfer) {
            indices.transfer_family = i;
        }

        // check for present capability
        VkBool32 present_support = device.getSurfaceSupportKHR(i, surface);
        if (present_support) {
            indices.present_family = i;
        }

        if (indices.is_complete()) { break; }
    }

    return indices;
}


bool Renderer::_check_device_extensions_support(vk::PhysicalDevice device) const
{
    std::vector<vk::ExtensionProperties> extensions = device.enumerateDeviceExtensionProperties();
    std::set<std::string> required_extensions(DEVICE_EXTENSIONS.begin(), DEVICE_EXTENSIONS.end());

    for (const vk::ExtensionProperties extension : extensions) {
        required_extensions.erase(extension.extensionName);
    }

    return required_extensions.empty();
}


void Renderer::_create_logical_device()
{
    // Queues
    QueueFamilyIndices indices = _find_queue_families(physical_device);
    float queue_priority = 1.0f;

    // using a set to remove duplicates (queue family serving multiple purposes)
    std::set queue_families {
        indices.graphics_family.value(),
        indices.present_family.value(),
        indices.compute_family.value(),
        indices.transfer_family.value(),
    };
    std::vector<vk::DeviceQueueCreateInfo> queueCreateInfos;
    for (uint32_t queue : queue_families) {
        queueCreateInfos.emplace_back(
            vk::DeviceQueueCreateInfo({}, indices.graphics_family.value(), 1, &queue_priority));
    };

    // device features
    vk::PhysicalDeviceFeatures deviceFeatures;

    // Finally create
    vk::DeviceCreateInfo createInfo(
        {},
        queueCreateInfos,
        nullptr,
        DEVICE_EXTENSIONS,
        &deviceFeatures);

    // Wait, let's enable validation layers if necessary
    if (enableValidationLayers) {
        createInfo.enabledLayerCount = static_cast<uint32_t>(VALIDATION_LAYERS.size());
        createInfo.ppEnabledLayerNames = VALIDATION_LAYERS.data();
    }

    // create
    device = physical_device.createDevice(createInfo);

    // initialize all queues references
    graphics_queue = device.getQueue(indices.graphics_family.value(), 0);
    compute_queue = device.getQueue(indices.compute_family.value(), 0);
    transfer_queue = device.getQueue(indices.transfer_family.value(), 0);
    present_queue = device.getQueue(indices.present_family.value(), 0);
}

void Renderer::_recreate_swap_chain()
{
    std::cout << "Recreating swapchain" << std::endl;
    device.waitIdle();

    _cleanup_swap_chain();

    _create_swap_chain();
    _create_swapchain_imageviews();
    _create_framebuffers();
    will_recreate_swap_chain = false;
}

void Renderer::_cleanup_swap_chain()
{
    for (vk::Framebuffer fb : swapChainFramebuffers)
        device.destroyFramebuffer(fb);
    for (vk::ImageView imageV : swapchain_imageviews)
        device.destroyImageView(imageV);
    device.destroySwapchainKHR(swapchain);
}

void Renderer::_create_swap_chain()
{
    SwapChainSupportDetails swapchain_support = _query_swapchain_support(physical_device);

    vk::SurfaceFormatKHR surface_format = _choose_swapsurface_format(swapchain_support.formats);
    vk::PresentModeKHR present_mode = _choose_swapsurface_present_mode(swapchain_support.presentModes);
    vk::Extent2D extent = _choose_swap_extent(swapchain_support.capabilities);

    uint32_t image_count = swapchain_support.capabilities.minImageCount + 1;
    if (swapchain_support.capabilities.maxImageCount > 0
     && image_count > swapchain_support.capabilities.maxImageCount)
    {
        image_count = swapchain_support.capabilities.maxImageCount;
    }

    QueueFamilyIndices indices = _find_queue_families(physical_device);
    std::array queue_family_indices { indices.graphics_family.value(), indices.present_family.value() };

    vk::SwapchainCreateInfoKHR createInfo (
        {}, surface,
        image_count,
        surface_format.format, surface_format.colorSpace,
        extent,
        // how many images for swap, 2 for stereoscopic vision
        1,
        // We will render directly to these images, so they are color attachments
        vk::ImageUsageFlagBits::eColorAttachment,
        // will be handled later, depending on the present and graphics queues setups
        vk::SharingMode::eExclusive,
        0,
        nullptr,
        // we can rotate 90 or mirror image here
        swapchain_support.capabilities.currentTransform,
        // how to composite with other windows (transparency?)
        // most likely not.
        vk::CompositeAlphaFlagBitsKHR::eOpaque,
        // present mode
        present_mode,
        // clipping: do not care about hidden pixels (unless we want to read from the screen, then we do care)
        VK_TRUE,
        // old swapchain - to recycle old one when it's outdated
        VK_NULL_HANDLE
    );

    if (indices.graphics_family != indices.present_family) {
        // exclusive is possible, concurrent is easier
        createInfo.imageSharingMode = vk::SharingMode::eConcurrent;
        createInfo.queueFamilyIndexCount = queue_family_indices.size();
        createInfo.pQueueFamilyIndices = queue_family_indices.data();
    } else {
        // exclusive better for performance
        createInfo.imageSharingMode = vk::SharingMode::eExclusive;
        createInfo.queueFamilyIndexCount = 1;
        createInfo.pQueueFamilyIndices = queue_family_indices.data();
    }

    swapchain = device.createSwapchainKHR(createInfo);

    // retrieve swapchain images
    swapchain_images = device.getSwapchainImagesKHR(swapchain);

    // save current swapchain configuration
    this->swapchain_image_format = surface_format.format;
    this->swapchain_extent = extent;
}


Renderer::SwapChainSupportDetails Renderer::_query_swapchain_support(vk::PhysicalDevice device) const
{
    SwapChainSupportDetails details;

    details.capabilities = device.getSurfaceCapabilitiesKHR(surface);
    details.formats = device.getSurfaceFormatsKHR(surface);
    details.presentModes = device.getSurfacePresentModesKHR(surface);

    return details;
}


vk::SurfaceFormatKHR Renderer::_choose_swapsurface_format(std::vector<vk::SurfaceFormatKHR> &availableFormats) const
{
    for (const vk::SurfaceFormatKHR format : availableFormats) {
        if (format.format == vk::Format::eR8G8B8A8Srgb
         && format.colorSpace == vk::ColorSpaceKHR::eSrgbNonlinear) {
            return format;
        }
    }
    return availableFormats[0];
}


vk::PresentModeKHR Renderer::_choose_swapsurface_present_mode(std::vector<vk::PresentModeKHR> &availablePresentModes) const
{
    for (const vk::PresentModeKHR mode : availablePresentModes) {
        if (mode == vk::PresentModeKHR::eMailbox) {
            return mode;
        }
    }
    // always guaranteed to be present
    return vk::PresentModeKHR::eFifo;
}


vk::Extent2D Renderer::_choose_swap_extent(const vk::SurfaceCapabilitiesKHR &capabilities) const
{
    if (capabilities.currentExtent.width != std::numeric_limits<uint32_t>::max()) {
        return capabilities.currentExtent;
    } else {
        int width, height;
        SDL_Vulkan_GetDrawableSize(window.get(), &width, &height);
        vk::Extent2D actualExtent(
            static_cast<uint32_t>(width),
            static_cast<uint32_t>(height)
        );

        actualExtent.width = std::clamp(actualExtent.width, capabilities.minImageExtent.width, capabilities.maxImageExtent.width);
        actualExtent.height = std::clamp(actualExtent.height, capabilities.minImageExtent.height, capabilities.maxImageExtent.height);

        return actualExtent;
    }
}


void Renderer::_create_swapchain_imageviews()
{
    swapchain_imageviews.resize(swapchain_images.size());
    for(uint32_t i=0; i<swapchain_images.size(); i++) {
        vk::ImageViewCreateInfo createInfo(
            {}, swapchain_images[i],
            vk::ImageViewType::e2D,
            swapchain_image_format,
            vk::ComponentMapping(vk::ComponentSwizzle::eIdentity, vk::ComponentSwizzle::eIdentity, vk::ComponentSwizzle::eIdentity, vk::ComponentSwizzle::eIdentity),
            vk::ImageSubresourceRange(vk::ImageAspectFlagBits::eColor, 0, 1, 0, 1)
        );

        swapchain_imageviews[i] = device.createImageView(createInfo);
    }
}


void Renderer::_create_graphics_pipeline()
{
    Shader simpleShader(device, "build/src/shaders/simple.vert.bin.spv", "build/src/shaders/simple.frag.bin.spv");

    vk::PipelineShaderStageCreateInfo vertShaderStageInfo({},
                                                            vk::ShaderStageFlagBits::eVertex,
                                                            simpleShader.vert_module,
                                                            "main");
    vk::PipelineShaderStageCreateInfo fragShaderStageInfo({},
                                                            vk::ShaderStageFlagBits::eFragment,
                                                            simpleShader.frag_module,
                                                            "main");
    std::array shaderStages {vertShaderStageInfo, fragShaderStageInfo};

    vk::VertexInputBindingDescription vertex_binding = Vertex::getBindingDescription();
    std::array vertex_attributes = Vertex::getAttributeDescriptions();
    vk::PipelineVertexInputStateCreateInfo vertexInputInfo({},
                                                           vertex_binding,
                                                           vertex_attributes);

    vk::PipelineInputAssemblyStateCreateInfo inputAssembly({},
                                                            vk::PrimitiveTopology::eTriangleList,
                                                            VK_FALSE);

    std::array viewports{ vk::Viewport(0, 0, swapchain_extent.width, swapchain_extent.height, 0, 1) };
    std::array scissors{ vk::Rect2D({0,0}, swapchain_extent) };
    vk::PipelineViewportStateCreateInfo viewportState({}, viewports, scissors);

    vk::PipelineRasterizationStateCreateInfo rasterizer({},
                                                        VK_FALSE,
                                                        VK_FALSE,
                                                        vk::PolygonMode::eFill,
                                                        vk::CullModeFlagBits::eBack,
                                                        vk::FrontFace::eClockwise,
                                                        VK_FALSE,
                                                        {},
                                                        {},
                                                        {},
                                                        1.0f);

    // Multisampling disabled
    vk::PipelineMultisampleStateCreateInfo multisampling({},
                                                         vk::SampleCountFlagBits::e1,
                                                         VK_FALSE,
                                                         {},
                                                         {},
                                                         {},
                                                         {});

    // Depth and Stencil
    //vk::PipelineDepthStencilStateCreateInfo depth_stencil_state();

    // Color blending (disabled)
    vk::PipelineColorBlendAttachmentState colorBlendAttachment(VK_FALSE,
                                                               vk::BlendFactor::eZero, vk::BlendFactor::eZero,
                                                               vk::BlendOp::eAdd,
                                                               vk::BlendFactor::eZero, vk::BlendFactor::eZero,
                                                               vk::BlendOp::eAdd,
                                                               vk::ColorComponentFlagBits::eR | vk::ColorComponentFlagBits::eG | vk::ColorComponentFlagBits::eB | vk::ColorComponentFlagBits::eA );
    vk::PipelineColorBlendStateCreateInfo colorBlending({},
                                                        VK_FALSE,
                                                        vk::LogicOp::eCopy,
                                                        colorBlendAttachment,
                                                        {0,0,0,0});

    // Dynamic viewport and scissor
    std::array dynamicStates {
        vk::DynamicState::eViewport,
        vk::DynamicState::eScissor
    };
    vk::PipelineDynamicStateCreateInfo dynamicState({}, dynamicStates );

    // Pipeline layout
    pipelineLayout = device.createPipelineLayout(vk::PipelineLayoutCreateInfo({}, nullptr, nullptr));
    // renderPass = simpleShader.createRenderPass(swapchain_image_format);

    vk::ResultValue<vk::Pipeline> result = device.createGraphicsPipeline(
        VK_NULL_HANDLE,
        vk::GraphicsPipelineCreateInfo({},
                                       shaderStages,
                                       &vertexInputInfo,
                                       &inputAssembly,
                                       nullptr,
                                       &viewportState,
                                       &rasterizer,
                                       &multisampling,
                                       nullptr,
                                       &colorBlending,
                                       &dynamicState, // nullptr to disable dynamic state
                                       pipelineLayout,
                                       renderPass,
                                       0,
                                       VK_NULL_HANDLE, -1)
    );
    if (result.result == vk::Result::ePipelineCompileRequiredEXT) {
        std::cerr << "WARNING!!! Pipeline compile Required!" << std::endl;
    }
    graphicsPipeline = result.value;
}

void Renderer::_create_render_pass()
{
    vk::AttachmentDescription colorAttachment({},
                                              swapchain_image_format,
                                              // multisampling disabled -> 1
                                              vk::SampleCountFlagBits::e1,
                                              // color image we clear and don't ignore the results
                                              vk::AttachmentLoadOp::eClear, vk::AttachmentStoreOp::eStore,
                                              // depth/stencil ignored
                                              vk::AttachmentLoadOp::eDontCare, vk::AttachmentStoreOp::eDontCare,
                                              // don't care initial layout, final layout to present
                                              vk::ImageLayout::eUndefined, vk::ImageLayout::ePresentSrcKHR);

    // This depends on the shaders source code:)
    uint32_t location=0;
    vk::AttachmentReference colorAttachmentRef(location, vk::ImageLayout::eColorAttachmentOptimal);

    // only one subpass for now
    vk::SubpassDescription subpass({},
                                   vk::PipelineBindPoint::eGraphics,
                                   nullptr,
                                   colorAttachmentRef,
                                   nullptr, nullptr, nullptr);

    vk::SubpassDependency dependency(VK_SUBPASS_EXTERNAL, 0,
                                     vk::PipelineStageFlagBits::eColorAttachmentOutput,
                                     vk::PipelineStageFlagBits::eColorAttachmentOutput,
                                     {}, //vk::AccessFlagBits::eColorAttachmentRead,
                                     vk::AccessFlagBits::eColorAttachmentWrite);

    renderPass = device.createRenderPass(vk::RenderPassCreateInfo(
        {},
        colorAttachment,
        subpass,
        dependency
    ));
}

void Renderer::_create_framebuffers()
{
    swapChainFramebuffers.resize(swapchain_imageviews.size());
    for (uint32_t i=0; i<swapchain_imageviews.size(); i++) {
        std::array attachments = { swapchain_imageviews[i] };

        swapChainFramebuffers[i] =
            device.createFramebuffer(vk::FramebufferCreateInfo(
                {}, renderPass, attachments, swapchain_extent.width, swapchain_extent.height, 1
            ));
    }
}


void Renderer::_create_command_pool()
{
    QueueFamilyIndices queueFamilyIndices = _find_queue_families(physical_device);

    commandPool = device.createCommandPool(vk::CommandPoolCreateInfo(
        vk::CommandPoolCreateFlagBits::eResetCommandBuffer, queueFamilyIndices.graphics_family.value()
    ));
}


void Renderer::_create_command_buffers()
{
    commandBuffers = device.allocateCommandBuffers(vk::CommandBufferAllocateInfo(
        commandPool, vk::CommandBufferLevel::ePrimary, MAX_FRAMES_IN_FLIGHT
    ));
}


void Renderer::_create_vertex_buffer()
{
    size_t size = sizeof(vertices[0])*vertices.size();
    Buffer cpubuffer = Buffer(device, physical_device, size,
                              vk::BufferUsageFlagBits::eTransferSrc | vk::BufferUsageFlagBits::eVertexBuffer,
                              vk::MemoryPropertyFlagBits::eHostVisible | vk::MemoryPropertyFlagBits::eHostCoherent);

    {
        Buffer::MappedBuffer mmap = cpubuffer.mmap();
        void* data = mmap.data();
        std::memcpy(mmap.data(), vertices.data(), size);
    }

    vertexBuffer = Buffer(device, physical_device, size,
                          vk::BufferUsageFlagBits::eTransferDst | vk::BufferUsageFlagBits::eVertexBuffer,
                          vk::MemoryPropertyFlagBits::eDeviceLocal);

    vertexBuffer.copyFrom(cpubuffer, commandPool, graphics_queue);
}


void Renderer::_create_index_buffer()
{
    size_t size = sizeof(indices[0])*indices.size();
    Buffer cpubuffer = Buffer(device, physical_device, size,
                              vk::BufferUsageFlagBits::eTransferSrc | vk::BufferUsageFlagBits::eIndexBuffer,
                              vk::MemoryPropertyFlagBits::eHostVisible | vk::MemoryPropertyFlagBits::eHostCoherent);

    {
        Buffer::MappedBuffer mmap = cpubuffer.mmap();
        void* data = mmap.data();
        std::memcpy(mmap.data(), indices.data(), size);
    }

    indexBuffer = Buffer(device, physical_device, size,
                          vk::BufferUsageFlagBits::eTransferDst | vk::BufferUsageFlagBits::eIndexBuffer,
                          vk::MemoryPropertyFlagBits::eDeviceLocal);

    indexBuffer.copyFrom(cpubuffer, commandPool, graphics_queue);
}



void Renderer::_create_sync_objects()
{
    vk::SemaphoreCreateInfo semaphoreInfo({}, nullptr);
    vk::FenceCreateInfo fenceInfo(
            // Create fence in signaled state, to not deadlock at the first frame
            vk::FenceCreateFlagBits::eSignaled
    );
    for (uint32_t i=0; i<MAX_FRAMES_IN_FLIGHT; i++) {
        imageAvailableSemaphores[i] = device.createSemaphore(semaphoreInfo);
        renderFinishedSemaphores[i] = device.createSemaphore(semaphoreInfo);
        inFlightFences[i] = device.createFence(fenceInfo);
    }
}


// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------


int Renderer::loop() {
    bool quit = false;
    while (!quit) {
        quit |= poll_events();
        render();
    }

    device.waitIdle();
    return 0;
}


void Renderer::render()
{
    // static size_t counter = 0;
    // std::cout << "Frame " << counter++ << std::endl;
    if (will_recreate_swap_chain) {
        _recreate_swap_chain();
    }

    // Wait for the previous frame to finish
    vk::resultCheck(device.waitForFences({inFlightFences[currentFrame]}, VK_TRUE, UINT64_MAX), "Error waiting for fence");

    // Acquire an image from the swap chain
    vk::ResultValue<uint32_t> result = device.acquireNextImageKHR(swapchain, UINT64_MAX, imageAvailableSemaphores[currentFrame], VK_NULL_HANDLE);
    uint32_t imageIndex = result.value;

    switch(result.result) {
        default:
            throw std::runtime_error("failed to acquire swap chain image");
        case vk::Result::eErrorOutOfDateKHR:
            // We can't render this, just try again at next drawframe call
            will_recreate_swap_chain = true;
            return;
        case vk::Result::eSuboptimalKHR:
            std::cout << "Swapchain suboptimal" << std::endl;
            // We should recreate the swapchain, but it's not mandatory
            will_recreate_swap_chain = true;
            //TODO We do not skip frame rendering, because the semaphore imageAvailableSemaphores[currentFrame] will complain otherwise
            break;
        case vk::Result::eSuccess:
            break;
    }

    // Reset the fence only if we are submitting work
    device.resetFences(inFlightFences[currentFrame]);

    // Record a command buffer which draws the scene onto that image
    commandBuffers[currentFrame].reset();
    _record_command_buffer(commandBuffers[currentFrame], imageIndex);

    // Submit the recorded command buffer
    std::array waitSemaphores {imageAvailableSemaphores[currentFrame]};
    std::array<vk::PipelineStageFlags,1> waitStages {vk::PipelineStageFlagBits::eColorAttachmentOutput};
    std::array signalSemaphores {renderFinishedSemaphores[currentFrame]};

    graphics_queue.submit({vk::SubmitInfo(
        waitSemaphores, waitStages, commandBuffers[currentFrame], signalSemaphores
    )}, inFlightFences[currentFrame]);

    // Present the swap chain image
    auto &presentWaitSemaphores = signalSemaphores;
    std::array swapchains { swapchain };
    std::array imageIndices { imageIndex };

    vk::Result present_result = present_queue.presentKHR(vk::PresentInfoKHR(
        presentWaitSemaphores, swapchains, imageIndices
    ));
    switch(present_result) {
        default:
            throw std::runtime_error("Couldn't present image to screen");
        case vk::Result::eSuboptimalKHR:
            will_recreate_swap_chain = true;
        case vk::Result::eSuccess:
            break;
    }

    currentFrame = (currentFrame + 1) % MAX_FRAMES_IN_FLIGHT;
}


void Renderer::_record_command_buffer(vk::CommandBuffer& commandBuffer, uint32_t imageIndex)
{
    // can throw exception
    commandBuffer.begin(vk::CommandBufferBeginInfo(vk::CommandBufferUsageFlagBits::eOneTimeSubmit, nullptr));

    std::array clearValue { vk::ClearValue(vk::ClearColorValue(0.5f, 0.3f, 0.5f, 1.0f)) };
    commandBuffer.beginRenderPass(
        vk::RenderPassBeginInfo(
            renderPass,
            swapChainFramebuffers[imageIndex],
            vk::Rect2D({0,0},swapchain_extent),
            clearValue
        ),
        vk::SubpassContents::eInline
    );
    commandBuffer.bindPipeline(vk::PipelineBindPoint::eGraphics, graphicsPipeline);

    // set viewport and scissor if dynamic
    commandBuffer.setViewport(0, std::array {vk::Viewport(0, 0, swapchain_extent.width, swapchain_extent.height, 0.0f, 1.0f)});
    commandBuffer.setScissor(0, std::array {vk::Rect2D({0,0}, swapchain_extent)});

    commandBuffer.bindVertexBuffers(0, {*vertexBuffer}, {0});
    commandBuffer.bindIndexBuffer(*indexBuffer, 0, vk::IndexType::eUint16);

    commandBuffer.drawIndexed(indices.size(), 1, 0, 0, 0);
    commandBuffer.endRenderPass();

    // can throw exception
    commandBuffer.end();
}


bool Renderer::poll_events()
{
    bool quit = false;
    SDL_Event event;
    while(SDL_PollEvent(&event)) {
        switch(event.type){
            case SDL_QUIT:
                quit = true;
                break;
            case SDL_WINDOWEVENT:
                switch (event.window.event) {
                    case SDL_WINDOWEVENT_CLOSE: // exit tests
                        quit = true;
                        break;
                    case SDL_WINDOWEVENT_RESIZED:
                        // Tells the engine to reload window configuration (size and dpi)
                        will_recreate_swap_chain = true;
                        break;
                    //TODO case SDL_WINDOWEVENT_FOCUS_LOST:
                    //     tests.is_window_active = false;
                    //     break;
                    //TODO case SDL_WINDOWEVENT_FOCUS_GAINED:
                    //     tests.is_window_active = true;
                    //     break;
                    default:
                        break;
                }
            default:
                break;
        }
    }
    return quit;
}


// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------


vk::DebugUtilsMessengerCreateInfoEXT Renderer::_configure_debug_messenger()
{
    return vk::DebugUtilsMessengerCreateInfoEXT(
        {},
        vk::DebugUtilsMessageSeverityFlagBitsEXT::eVerbose
        |vk::DebugUtilsMessageSeverityFlagBitsEXT::eInfo
        |vk::DebugUtilsMessageSeverityFlagBitsEXT::eWarning
        |vk::DebugUtilsMessageSeverityFlagBitsEXT::eError
        ,
        vk::DebugUtilsMessageTypeFlagBitsEXT::eGeneral
        |vk::DebugUtilsMessageTypeFlagBitsEXT::eValidation
        |vk::DebugUtilsMessageTypeFlagBitsEXT::ePerformance
        |vk::DebugUtilsMessageTypeFlagBitsEXT::eDeviceAddressBinding
        ,
        &debugCallback,
        this
    );
}


void Renderer::_setup_debug_messenger()
{
    if (!enableValidationLayers) return;

    vk::DebugUtilsMessengerCreateInfoEXT createInfo = _configure_debug_messenger();
    vk::resultCheck(instance.createDebugUtilsMessengerEXT(&createInfo,nullptr,&debugMessenger),
                    "Failed to setup debug callback");

}


vk::Bool32 Renderer::debugCallback(
        VkDebugUtilsMessageSeverityFlagBitsEXT pMessageSeverity,
        VkDebugUtilsMessageTypeFlagsEXT pMessageType,
        const VkDebugUtilsMessengerCallbackDataEXT* pCallbackData,
        void* pUserData)
{
    Renderer* _this = static_cast<Renderer*>(pUserData);
    if (pMessageSeverity >= _this->verbositiy_level) {
        if      (pMessageSeverity & VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT  ) std::cerr << "[ ERROR ]";
        else if (pMessageSeverity & VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT) std::cerr << "[Warning]";
        else if (pMessageSeverity & VK_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT   ) std::cerr << "[  Info ]";
        else if (pMessageSeverity & VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT) std::cerr << "[Verbose]";
        std::cerr << " validation layer: " << pCallbackData->pMessage << std::endl;
    }
    return VK_FALSE;
}
